package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"time"
)

const dbPath = "./downloader.db"

type DownloadedFile struct {
	gorm.Model
	OriginalURL   string
	LocalName     string
	FileExtension string
	FileSize      uint64
	DownloadDate  time.Time
}

func setupHistory() error {
	db, err := gorm.Open("sqlite3", dbPath)
	if err != nil {
		return fmt.Errorf("error on connecting to db path: '%s', Err: %s", dbPath, err.Error())
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&DownloadedFile{})

	return nil
}

func addFileToHistory(file *DownloadedFile) {
	db, err := gorm.Open("sqlite3", dbPath)
	if err != nil {
		log.Printf("error on connecting to db path: '%s', Err: %s\n", dbPath, err.Error())
		return
	}
	defer db.Close()
	db.Create(file)
}

func getAllFileHistory() (result []DownloadedFile, err error) {
	db, err := gorm.Open("sqlite3", dbPath)
	if err != nil {
		return nil, fmt.Errorf("error on connecting to db path: '%s', Err: %s\n", dbPath, err.Error())
	}
	defer db.Close()
	db.Find(&result)
	return
}
