package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
)

func getHandleApiMux() *http.ServeMux {
	mux := http.NewServeMux()

	mux.HandleFunc("/upload", func(w http.ResponseWriter, r *http.Request) {

		if err := r.ParseMultipartForm(1 << 26); err != nil {
			log.Printf("Error on parseMultiPart of upload, Err: %s\n", err.Error())
			http.Error(w, "Error on parsing multipart form", http.StatusInternalServerError)
			return
		}

		uploadedFile, header, err := r.FormFile("file")
		if err != nil {
			log.Printf("Error on get file of upload, Err: %s\n", err.Error())
			http.Error(w, "Error on getting file from form", http.StatusInternalServerError)
			return
		}
		defer uploadedFile.Close()

		file, err := os.OpenFile(fmt.Sprintf("%s/user-content/%s", imagesFolderPath, header.Filename), os.O_CREATE|os.O_WRONLY, os.ModePerm)
		if err != nil {
			log.Printf("Error on opening uploaded file, Err: %s\n", err.Error())
			http.Error(w, "Error on opening uploaded file", http.StatusInternalServerError)
			return
		}
		defer file.Close()

		if _, err := io.Copy(file, uploadedFile); err != nil {
			log.Printf("Error on writing uploaded file, Err: %s\n", err.Error())
			http.Error(w, "Error on writing uploaded file", http.StatusInternalServerError)
			return
		}
		http.Error(w, "Ok", http.StatusOK)
	})

	mux.HandleFunc("/addUrlToQ", func(w http.ResponseWriter, r *http.Request) {
		link := r.URL.Query().Get("URL")
		if link == "" {
			http.Error(w, "URL query must be provided", http.StatusBadRequest)
			return
		}
		if u, err := url.ParseRequestURI(link); err != nil {
			http.Error(w, fmt.Sprintf("Bad URL: '%s', Err: %s\n", link, err.Error()), http.StatusBadRequest)
		} else {
			addToQueue(u)
		}
		http.Error(w, "Ok", http.StatusOK)
	})

	mux.HandleFunc("/getHistory", func(w http.ResponseWriter, r *http.Request) {
		files, err := getAllFileHistory()
		if err != nil {
			log.Printf("Error on getAllFileHistory, Err: %s\n", err.Error())
			http.Error(w, "Error on getting history", http.StatusInternalServerError)
			return
		}

		type getHistoryResult struct {
			Files []DownloadedFile
		}
		result := getHistoryResult{files}

		w.Header().Set("Content-Type", "application/json")

		if err := json.NewEncoder(w).Encode(result); err != nil {
			log.Printf("Error on encode result on getHistory, Err: %s\n", err.Error())
			http.Error(w, "Error on encoding history result", http.StatusInternalServerError)
			return
		}
	})

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
	})

	return mux
}
