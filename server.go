package main

import (
	"fmt"
	"io"
	"log"
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

func runHttpServer() {
	mux := http.NewServeMux()

	mux.Handle("/api/", http.StripPrefix("/api", getHandleApiMux()))

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		handleStatic(w, r)
	})

	mux.Handle("/files/", http.StripPrefix("/files", http.FileServer(http.Dir(imagesFolderPath))))

	withOptionsHandler := http.NewServeMux()
	withOptionsHandler.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		if r.Method == http.MethodOptions {
			return
		}
		mux.ServeHTTP(w, r)
	})
	if err := http.ListenAndServe(serverListenAddr, withOptionsHandler); err != nil {
		log.Fatalf("Error on listening on %s, Err: %s\n", serverListenAddr, err.Error())
	}
}

func handleStatic(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	isIndex := false
	path := filepath.Clean(r.URL.Path)
	if path == "/" { // Add other paths that you route on the UI side here
		path = "index.html"
	}
	if strings.HasSuffix(path, "index.html") {
		isIndex = true
	}
	path = strings.TrimPrefix(path, "/")

	file, err := uiFS.Open(path)
	if err != nil {
		if os.IsNotExist(err) {
			file, err = uiFS.Open("index.html")
			if err != nil && os.IsNotExist(err) {
				log.Println("file", path, "not found:", err)
				http.NotFound(w, r)
				return
			}
			isIndex = true
		}
		if err != nil {
			log.Println("file", path, "cannot be read:", err)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
	}

	contentType := mime.TypeByExtension(filepath.Ext(path))
	w.Header().Set("Content-Type", contentType)
	if strings.HasPrefix(path, "assets/") {
		w.Header().Set("Cache-Control", "public, max-age=86400")
	} else if isIndex {
		w.Header().Set("Cache-Control", "no-store")
	}
	stat, err := file.Stat()
	if err == nil && stat.Size() > 0 {
		w.Header().Set("Content-Length", fmt.Sprintf("%d", stat.Size()))
	}

	_, _ = io.Copy(w, file)
}
