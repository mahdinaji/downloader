package main

import (
	"fmt"
	"io"
	"log"
	"mime"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	humanize "github.com/dustin/go-humanize"
)

var client = http.Client{}

func init() {
	client.Timeout = 30 * time.Second
}

func runWorker(name string) {
	for {
		select {
		case u := <-downloadQueue:
			if size, spent, err := download(u); err != nil {
				log.Printf("a download failed by worker %s, Err: %s\n", name, err.Error())
			} else {
				log.Printf("downloading '%s' finished at %v by worker %s, filesize: %s\n", u.String(), spent.String(), name, humanize.Bytes(size))
			}
		}
	}
}

func setupWorkers(count int) {
	for i := 0; i < count; i++ {
		go runWorker(fmt.Sprintf("#%d", i+1))
	}
}

func download(u *url.URL) (filesize uint64, spent time.Duration, err error) {
	startTimestamp := time.Now()
	defer func() {
		spent = time.Now().Sub(startTimestamp)
	}()

	filename := checkFilenameDuplicate(path.Base(u.Path))

	resp, err := client.Get(u.String())
	if err != nil {
		return 0, 0, fmt.Errorf("error on get file: %s, Err: %s\n", filename, err.Error())
	}
	defer resp.Body.Close()

	// check if file name exists in response
	contentDisposition := resp.Header.Get("Content-Disposition")
	if _, params, err := mime.ParseMediaType(contentDisposition); err == nil {
		filename = checkFilenameDuplicate(params["filename"])
	}

	file, err := os.Create(filename)
	if err != nil {
		return 0, 0, fmt.Errorf("error on opening result file: %s, Err: %s\n", filename, err.Error())
	}
	defer file.Close()

	if n, err := io.Copy(file, resp.Body); err != nil {
		return 0, 0, fmt.Errorf("Error on write file: %s, Err: %s\n", filename, err.Error())
	} else {
		filesize = uint64(n)
	}

	addFileToHistory(&DownloadedFile{
		OriginalURL:   u.String(),
		LocalName:     path.Base(filename),
		FileExtension: filepath.Ext(filename),
		FileSize:      filesize,
		DownloadDate:  time.Now(),
	})

	return filesize, 0, nil
}

func checkFilenameDuplicate(filename string) string {
	newPath := fmt.Sprintf("%s/%s", imagesFolderPath, filename)
	trials := 1
	for {
		if _, err := os.Stat(newPath); os.IsNotExist(err) {
			break
		}
		trials++
		// file already exists
		newPath = fmt.Sprintf("%s/%s-%d%s", imagesFolderPath, strings.TrimSuffix(filename, filepath.Ext(filename)), trials, filepath.Ext(filename))
	}
	return newPath
}
