module downloader

go 1.18

require (
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.16 // indirect
)
