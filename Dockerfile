FROM node:latest AS UI_Build
WORKDIR /downloader_ui/
COPY ./ui/package.json .
RUN npm install
COPY ./ui/ .
RUN npm run build

FROM golang:1.18-alpine3.17 AS Go_Build
RUN apk add build-base
WORKDIR /go/src/downloader/
COPY --from=UI_Build /downloader_ui/build/ ./ui/build/
COPY . .
RUN go mod download
RUN go build -o downloader .

FROM alpine:latest
WORKDIR /root/
COPY --from=Go_Build /go/src/downloader/downloader ./
COPY links.txt ./
CMD ["./downloader"]