package main

import "net/url"

var downloadQueue chan *url.URL

func init() {
	downloadQueue = make(chan *url.URL, 100)
}

func addToQueue(url *url.URL) {
	downloadQueue <- url
}
