package main

import (
	"bufio"
	"fmt"
	"log"
	"net/url"
	"os"
	"strings"
)

var linksPathname = "./links.txt"

func downloadDefaultFiles() error {
	// override default links.txt file path
	if len(os.Args) > 1 {
		linksPathname = os.Args[1]
	}

	linksFile, err := os.Open(linksPathname)
	if err != nil {
		return fmt.Errorf("error on opening file: '%s', Err: %s", linksPathname, err.Error())
	}
	defer linksFile.Close()

	scanner := bufio.NewScanner(linksFile)
	for scanner.Scan() {
		link := strings.TrimSpace(scanner.Text())
		if link == "" {
			continue
		}
		if u, err := url.ParseRequestURI(link); err != nil {
			log.Printf("Bad URL: '%s', Err: %s\n", link, err.Error())
			continue
		} else {
			addToQueue(u)
		}
	}

	return nil
}
