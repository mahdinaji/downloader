# downloader

A Downloader application written in Go. It creates 5 workers that download URLs coming from a channel.
On startup, a file named links.txt alongside with the binary file will be read and the links will be sent through that channel. The results will be saved in images folder.
Whenever it downloads a file, it logs the details (download history) in a sqlite3 file named 'downloader.db'.

A ReactJS UI is embedded in the binary file. It's served through port number 7996 with the API. The panel gives access to the history db and allows you to download the files.

You can also upload a file using the UI, the file will placed at ./images/user-content/ folder alongside with the binary file.

## How to Run
You have two options to run this app:
1. Using Dockerfile
2. Build and Run the main binary file

### 1. Using Dockerfile
Simply build the Dockerfile and then run it with publishing port number 7996.
```shell
foo@bar:~$ cd ./downloader
foo@bar:~$ docker build -t downloader:latest .
foo@bar:~$ docker run --rm -p 7996:7996 downloader:latest
```
Then go to http://localhost:7996/

### 2. Build and Run the main binary file
First you need to build the UI. Clone the project and go to ./ui folder to install the dependencies and build the project.
```shell
foo@bar:~$ cd ./downloader/ui/
foo@bar:~$ npm install
foo@bar:~$ npm run build
```
The project will be built and placed at ./ui/build/ folder.
Then you can go ahead and build the main binary file.

```shell
foo@bar:~$ cd ./downloader
foo@bar:~$ go build -o downloader .
foo@bar:~$ ./downloader links.txt
```



## API

There are 3 API endpoints and they've been used by the UI.

### 1. /api/upload
If the content type is multipart/form, it will take the 'file' named parameter and save it to ./images/user-content/ folder.

### 2. /api/addUrlToQ?URL=<URL to download>
If you send a request with 'URL' query string with GET method, it'll send the URL to that channel to download it.

### 3. /api/getHistory
It returns a JSON response containing all the files downloaded before. The files are in an Array named Files in the JSON object.

