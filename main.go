package main

import (
	"downloader/utils"
	"embed"
	"io/fs"
	"log"
)

const imagesFolderPath = "./images"
const serverListenAddr = ":7996"

//go:embed ui/build
var UI embed.FS
var uiFS fs.FS

func init() {
	var err error
	uiFS, err = fs.Sub(UI, "ui/build")
	if err != nil {
		log.Fatal("failed to get ui fs", err)
	}
}

func main() {
	log.Println("Downloader Started...")

	// Mkdir images folder
	if err := utils.MkDir(imagesFolderPath); err != nil {
		log.Fatalf("Error on mkdir '%s' folder, Err: %s\n", imagesFolderPath, err.Error())
	}
	if err := utils.MkDir(imagesFolderPath + "/user-content"); err != nil {
		log.Fatalf("Error on mkdir '%s/user-content' folder, Err: %s\n", imagesFolderPath, err.Error())
	}

	if err := setupHistory(); err != nil {
		log.Fatalf("Error on setup history, Err: %s\n", err.Error())
	}

	setupWorkers(5)

	if err := downloadDefaultFiles(); err != nil {
		log.Println(err.Error())
	}

	runHttpServer()
}
