import axios from "axios";
import react, { useState } from "react";
import "./App.css";

var API_URL = window.localStorage.getItem("DOWNLOADER_API_URL")
  ? window.localStorage.getItem("DOWNLOADER_API_URL")
  : `${window.location.origin}/api`;

function Download() {
  const [url, setUrl] = useState("");

  const download = () => {
    if (url === "") {
      window.alert("URL cannot be empty!");
      return;
    }
    axios
      .get(`${API_URL}/addUrlToQ`, { params: { URL: url } })
      .then((response) => {
        setUrl("");
        window.alert("Added to Download Queue");
      })
      .catch((reason) => {
        console.log("Error on Adding to Download Queue", reason);
        window.alert(`Error on Adding to Download Queue, Err: ${reason.message}`);
      });
  };

  return (
    <div className="Panel">
      <div style={{ padding: "0 10px" }}>Download URL</div>
      <div style={{ padding: "5px" }}>
        <input
          type={"text"}
          value={url}
          placeholder={"https://google.com/"}
          onChange={(e) => setUrl(e.target.value)}
        />
      </div>
      <div
        style={{ display: "flex", justifyContent: "right", padding: "0 5px" }}
      >
        <button style={{ padding: "5px" }} onClick={download}>
          Download
        </button>
      </div>
    </div>
  );
}

export default Download;
