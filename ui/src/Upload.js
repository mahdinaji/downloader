import axios from "axios";
import react, { useState } from "react";
import "./App.css";

var API_URL = window.localStorage.getItem("DOWNLOADER_API_URL")
  ? window.localStorage.getItem("DOWNLOADER_API_URL")
  : `${window.location.origin}/api`;

function Upload() {
  const [selectedFile, setSelectedFile] = useState(null);
  const [uploading, setUploading] = useState(false);

  const upload = () => {
    if (uploading) {
      return;
    }

    if (!selectedFile) {
      window.alert("Choose a file first!");
      return;
    }

    setUploading(true);
    const formData = new FormData();
    formData.append("file", selectedFile);
    axios
      .post(`${API_URL}/upload`, formData, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((response) => {
        setSelectedFile();
        window.alert("File uploaded.");
      })
      .catch((reason) => {
        console.log("Error on Uploading File", reason);
        window.alert(`Error on Uploading File, Err: ${reason.message}`);
      })
      .finally(() => {
        setUploading(false);
      });
  };

  return (
    <div className="Panel">
      <div style={{ padding: "0 10px" }}>Upload URL</div>
      <div style={{ padding: "5px" }}>
        <input
          type={"file"}
          onChange={(e) => setSelectedFile(e.target.files[0])}
        />
      </div>
      <div
        style={{ display: "flex", justifyContent: "right", padding: "0 5px" }}
      >
        <a
          href={`${API_URL.replace("/api", "/files/user-content/")}`}
          target={"_blank"}
          style={{ fontSize: "0.9rem", margin: "5px" }}
        >
          See Uploaded files
        </a>
        <button
          style={{ padding: "5px" }}
          disabled={uploading}
          onClick={upload}
        >
          Upload
        </button>
      </div>
    </div>
  );
}

export default Upload;
