import react, { useState, useEffect } from "react";
import "./App.css";
import MaterialReactTable from "material-react-table";
import axios from "axios";

var API_URL = window.localStorage.getItem("DOWNLOADER_API_URL")
  ? window.localStorage.getItem("DOWNLOADER_API_URL")
  : `${window.location.origin}/api`;

const columns = [
  {
    accessorKey: "LocalName",
    header: "Local Name",
  },
  {
    accessorKey: "FileExtension",
    header: "File Extension",
  },
  {
    accessorKey: "FileSize",
    header: "File Size",
    Cell: ({ cell }) => `${humanFileSize(cell.getValue())}`,
  },
  {
    accessorKey: "DownloadDate",
    header: "Downloaded At",
  },
  {
    accessorKey: "OriginalURL",
    header: "Original URL",
  },
];

function History() {
  const [data, setData] = useState();

  useEffect(() => {
    axios
      .get(`${API_URL}/getHistory`)
      .then((response) => {
        setData(response.data.Files);
      })
      .catch((reason) => {
        console.log("Error on getting history", reason);
        window.alert(`Error on getting History, Err: ${reason.message}`);
      });
  }, []);

  return (
    <div className="Panel" style={{ minWidth: "60vw" }}>
      <div style={{ padding: "5px 10px" }}>Download History</div>
      {data ? (
        <MaterialReactTable
          columns={columns}
          data={data}
          enableRowActions
          renderRowActionMenuItems={({ row }) => (
            <a
              style={{ padding: "10px" }}
              download
              target={"_blank"}
              href={`${API_URL.replace("/api", "/files")}/${
                row.original.LocalName
              }`}
            >
              Download file
            </a>
          )}
          initialState={{ columnVisibility: { OriginalURL: false } }}
        />
      ) : (
        <div style={{ textAlign: "center", margin: "10px" }}>Loading...</div>
      )}
    </div>
  );
}

function humanFileSize(bytes) {
  const thresh = 1024;

  if (Math.abs(bytes) < thresh) {
    return bytes + " B";
  }

  const units = ["KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"];
  let u = -1;
  const r = 10;

  do {
    bytes /= thresh;
    ++u;
  } while (
    Math.round(Math.abs(bytes) * r) / r >= thresh &&
    u < units.length - 1
  );

  return bytes.toFixed(1) + " " + units[u];
}

export default History;
