import './App.css';
import Download from './Download';
import History from './History';
import Upload from './Upload';

function App() {
  return (
    <div className="App">
      <Download />
      <Upload />
      <History />
    </div>
  );
}

export default App;
