package utils

import (
	"fmt"
	"os"
)

func MkDir(path string) error {
	if folderInfo, err := os.Stat(path); os.IsNotExist(err) {
		if err := os.Mkdir(path, 0744); err != nil {
			return fmt.Errorf("error on mkdir folder: '%s', Err: %s", path, err.Error())
		}
	} else if err != nil {
		return fmt.Errorf("error on get folder: '%s' stat, Err: %s", path, err.Error())
	} else {
		if !folderInfo.IsDir() {
			return fmt.Errorf("path: '%s' is not a directory", path)
		}
	}
	return nil
}
